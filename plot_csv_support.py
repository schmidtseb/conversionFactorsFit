# Import modules
import pandas as pd
import numpy as np
import scipy.optimize
import csv
import os
import collections
import re

# Matplotlib & Seaborn
from matplotlib import pyplot as plt
import seaborn as sns
from matplotlib.colors import ListedColormap

# Support modules
from uncertainties import ufloat
from collections import OrderedDict
from pyexcel_ods import save_data
from IPython.display import display, Markdown, display_html

# Support function to display two dataframes side by side
def display_side_by_side(*args):
    html_str=''
    for df in args:
        html_str+=df.to_html()
    display_html(html_str.replace('table','table style="display:inline"'),raw=True)

# Combine the entries of all small and large pixels
def combineBins(df, large=True):    
    smallIdx, largeIdx = 4, 12
    idx = 2
    
    smallDf = pd.DataFrame()
    largeDf = pd.DataFrame()
    
    smallDf = smallDf.append(df.iloc[0:idx])
    while idx < 255:
        largeDf = largeDf.append(df.iloc[idx:idx+largeIdx])
        idx += largeIdx
        smallDf = smallDf.append(df.iloc[idx:idx+smallIdx])
        idx += smallIdx
        
    if large:        
        df = largeDf.sum(axis=0)
    else:
        df = smallDf.sum(axis=0)
    
    # Statistics of first five bins
    mu = np.mean( df[0:5] )
    sigma = np.mean( df[0:5] )
    
    # Loop over the bins
    for i in range(5):
        if abs(df[i] - mu) > 3*sigma:
            df[i] = 0
    
    return df
    
# Return the combined pixel data for all files in the input folder
def getCombinedBins(inFolder, erlangen=False, large=True, angle=False, smallOpt=False):
    dataDict = {'Slot1': {}, 'Slot2': {}, 'Slot3': {}}
    for fn in os.listdir(inFolder):
        if os.path.isfile(inFolder + fn) and fn.endswith('.csv'):
            if not angle:
                if smallOpt:
                    # Get slot number (single digit)
                    slotType = 'Slot' + fn.split('Slot')[-1][0]
                    # In case of the optimization of small pixels,
                    # energy refers to the primary energy of the X-ray tube
                    splitFn = fn.split('_')
                    for s in splitFn:
                        if 'kV' in s:
                            energy = s.split('kV')[0]
                else:
                    nameSplit = fn.split('.')[0].split('_')
                    slotType, energy = nameSplit[-1], nameSplit[0]
                    if erlangen:
                        slotType, energy = energy, slotType
            else:
                nameSplit = fn.split('.csv')[0].split('_')
                energy, angle, slotType = nameSplit
            
            energy = int(re.sub("\D","", energy)) # int(energy.split('kV')[0])
            # print(slotType, energy)

            df = pd.read_csv(inFolder + fn, header=None)
            df = combineBins(df, large=large)

            if angle:
                dataDict[slotType][(energy, int(angle))] = df
            else:
                dataDict[slotType][energy] = df

    return dataDict

# Combine the number of counts into one list in the returned *dataDict* of *getCombinedBins()*
def getCounts(dataDict, energy):
    countList = []
    for i in range(1, 3 + 1):
        countList += list(dataDict['Slot%d' % i][energy])
    
    return countList

# Wrapper for getCounts
def getCountsWrap(inFolder, energyFile, header, erlangen=False, large=True, angle=False, smallOpt=False):
    doseDf = pd.read_table(inFolder + energyFile, comment='#')
    # doseDf[doseDf < 0] = np.nan
    # display( doseDf )

    energyHead = header[0]
    energyRealHead = header[1]
    trueDoseHead = header[2]
    
    energyIdx = list( doseDf[energyHead] )
    realDose = list( doseDf[trueDoseHead] )
    energyListFile = list( doseDf[energyRealHead] )   
    if angle:
        angleHead = header[3]
        angleList = list( doseDf[angleHead] )
        
    realDose, energyListFile = zip(*[val for val in zip(realDose, energyListFile) if not np.isnan(val[0])])    
    
    dataDict = getCombinedBins(inFolder, erlangen=erlangen, large=large, angle=angle, smallOpt=smallOpt)
    energyList = []
    energyCounts = []

    # for key in dataDict['Slot1']:
    for i in range(len(energyIdx)):
        if angle:
            key = (energyIdx[i], angleList[i])
        else:
            key = round(energyIdx[i], 1)

        energyList.append( key )
        energyCounts.append( getCounts(dataDict, key) )
    energyCounts = np.array( energyCounts )
    energyCountsErr = np.sqrt( energyCounts )
    
    if angle:
        return energyList, energyListFile, energyCounts, energyCountsErr, realDose, angleList
    else:
        return energyList, energyListFile, energyCounts, energyCountsErr, realDose
    
def getCountsAngle(inFolder, energyFile, header, erlangen=False, large=True):
    doseDf = pd.read_table(inFolder + energyFile, comment='#')
    doseDf[doseDf < 0] = np.nan
    display( doseDf )
    
    energyHead = header[0]
    trueDoseHead = header[1]
    angleHead = header[2]
    realDose = list( doseDf[trueDoseHead] )
    energyListFile = list( doseDf[energyHead] )
    realDose, energyListFile = zip(*[val for val in zip(realDose, energyListFile) if not np.isnan(val[0])])    
    
    dataDict = getCombinedBins(inFolder, erlangen=erlangen, large=large)

# Load conversion factors from file
def getConversionFactorsFile(inFile):
    df = pd.read_csv(inFile, header=None)
    conversionList = []
    for i in range(3):
        conversionList += list( df[i] )
        
    return np.array( conversionList )

# === FIT FUNCTIONS ===
# Fit conversion factors
def fitConversionNormal(H, y, alphaBounds, alphaInit, werr=False):
    def fitFunc(x, H, y, werr=werr):
        if werr:
            return 1./np.sqrt(np.dot(H, np.square(x))) * np.abs(np.dot(H, x) - y)
        else:
            return np.abs(np.dot(H, x) - y) + np.sqrt(np.dot(H, np.square(x)))

    alphaInit[alphaInit < 0] = 0
    return scipy.optimize.least_squares(fitFunc, x0=np.asarray(alphaInit), args=(H, y), bounds=alphaBounds)['x']
    
# See https://scicomp.stackexchange.com/questions/10671/tikhonov-regularization-in-the-non-negative-least-square-nnls-pythonscipy  
# for fit of underdetermined matrix
def fitConversion(H, y, gamma=.1, minBound=1.e-6, alphaBounds=False, alphaInit=None, werr=False):
    C = np.concatenate((H, gamma*np.identity(len(H[0]))), axis=0)
    d = np.concatenate((y, np.zeros(len(H[0]))), axis=0)
    
    def fitFunc(x, H, y, werr=werr):
        if werr:
            return 1./np.sqrt(np.dot(H, np.square(x))) * np.abs(np.dot(H, x) - y)
        else:
            # Second summand added to minimize statistical error of reconstructed dose
            return np.abs(np.dot(H, x) - y) + np.sqrt(np.dot(H, np.square(x)))
    
    if alphaBounds and list(alphaInit):
        alphaInit[alphaInit < 0] = 0
        # return scipy.optimize.lsq_linear(C, d, bounds=alphaBounds)['x']
        return scipy.optimize.least_squares(fitFunc, x0=np.array(alphaInit), args=(C, d), bounds=alphaBounds)['x']
    elif minBound == 0:
        return scipy.optimize.nnls(C, d)[0]
    else:
        return scipy.optimize.lsq_linear(C, d, bounds=(minBound, np.inf))['x']
    
# \vec\alpha_\mathrm{tot} = \sum_i k_i \vec\alpha_i
# \sum_i^N k_i = 1 \rightarrow k_N = 1 - \sum_i^{N-1}k_i
def getAlphaTot(alphaList, kList):
    if len(alphaList) == 1:
        return alphaList[0]
    
    # return (np.asarray(alphaList[:-1]) * np.asarray(kList) + np.asarray(alphaList[-1]) * (1 - sum(kList)))[0]
    return (np.asarray([a*kList[i] for i, a in enumerate(alphaList[:-1])]) + np.asarray(alphaList[-1]) * (1 - sum(kList)))[0]
    
# Fit conversion factors using simulated and measured data at the same time
def fitConversionSim(H, y, energyMatrixList, doseVectorList, alphaInit=None, werr=False):
    assert len(energyMatrixList) == len(doseVectorList)
         
    def getAlpha_k(x, size):
        if size == 1:
            return [x[:-1]], [x[-1]]
        
        l = int( (len(x)-(size - 1)) / size )
        alphaList = []
        for i in range(size):
            alphaList.append(np.asarray(x[i*l:(i+1)*l]))
        kList = x[-(size - 1):]
        return alphaList, kList
        
    def fitFunc(x, H, y, energyMatrixList, doseVectorList, werr=werr):
        if werr:
            return 1./np.sqrt(np.dot(H, np.square(x))) * np.abs(np.dot(H, x) - y)
        else:
            # l: length of one alpha
            l = int( (len(x)-(len(energyMatrixList) - 1)) / len(energyMatrixList) )
            alphaList, kList = getAlpha_k(x, len(energyMatrixList))
            # x1, x2, k = x[:l], x[l:-1], x[-1]
            
            # TODO: Remove angle fix
            '''
            for i in range(len(energyMatrixList)):
                alphaList[i][14] = 0
                alphaList[i][15] = 0
            '''
            
            # Calculate total alpha
            alphaTot = getAlphaTot(alphaList, kList)
            
            # Fit to the measured data            
            sum = np.linalg.norm(np.dot(H, alphaTot) - y)
            for i in range(len(energyMatrixList)):
                sum += np.linalg.norm(np.dot(energyMatrixList[i], alphaList[i]) - doseVectorList[i])
                
            return sum
            # Second summand added to minimize statistical error of reconstructed dose
            # return np.abs(np.dot(H, x) - y) + np.sqrt(np.dot(H, np.square(x)))
    
    size = len(energyMatrixList)
    x = scipy.optimize.least_squares(fitFunc, x0=np.array(list(alphaInit)*size + [1./size]*size), args=(H, y, energyMatrixList, doseVectorList), bounds=(0, np.infty))['x']
    l = int((len(x)-1)/2.)
    alphaList, kList = getAlpha_k(x, len(energyMatrixList))
    # x1, x2, k = x[:l], x[l:-1], x[-1]
    return alphaList, kList

# === SUPPORT FUNCTIONS ===
def getEnergyCounts(inFolder, large=True, smallOpt=False):
    dataDict = getCombinedBins(inFolder, large=large, smallOpt=smallOpt)
    energyList = []
    energyCounts = []

    for key in sorted(dataDict['Slot1']):
        energyList.append( key )
        energyCounts.append( getCounts(dataDict, key) )
    energyCounts = np.array( energyCounts )
    outDict = {'energyList': energyList, 'energyCounts': energyCounts}
    
    return outDict

def getDose(energyCounts, conversionFactors):
    energyCountsErr = np.sqrt( energyCounts )
    
    dose = np.dot(energyCounts, conversionFactors)
    doseErr = np.sqrt( np.dot(np.square(energyCountsErr), np.square(conversionFactors)) )
    doseUnc = [ufloat(dose[i], doseErr[i]) for i in range(len(dose))]
    
    doseDict = {'dose': dose, 'doseErr': doseErr, 'doseUnc': doseUnc}
    return doseDict

def getDoseSingle(energyCounts, conversionFactors):
    energyCountsErr = np.sqrt( energyCounts )
    
    # Split conversion factors into three
    convLen = len( conversionFactors )
    conversionFactorsSplit = [conversionFactors[:convLen//3], conversionFactors[convLen//3:2*convLen//3], conversionFactors[2*convLen//3:]]
    energyCounts = np.asarray(energyCounts)
    energyCountsErr = np.asarray(energyCountsErr)
    energyCountsSplit = [energyCounts[:, range(convLen//3)], energyCounts[:, range(convLen//3, 2*convLen//3)], energyCounts[:, range(2*convLen//3, convLen)]]
    energyCountsErrSplit = [energyCountsErr[:, range(convLen//3)], energyCountsErr[:, range(convLen//3, 2*convLen//3)], energyCountsErr[:, range(2*convLen//3, convLen)]]
    
    doseDictList = []
    for k in range(3):
        dose = np.dot(energyCountsSplit[k], conversionFactorsSplit[k])
        doseErr = np.sqrt( np.dot(np.square(energyCountsErrSplit[k]), np.square(conversionFactorsSplit[k])) )
        doseUnc = [ufloat(dose[i], doseErr[i]) for i in range(len(dose))]
        doseDict = {'dose': dose, 'doseErr': doseErr, 'doseUnc': doseUnc}

        doseDictList.append( doseDict )
        
    return doseDictList

def getTotalEnergy(energyCounts, binEdges):
    binEdgeList = []
    for key in ['vac', 'al', 'sn']:
        binEdgeList += list( np.asarray(binEdges[key][:-1]) + np.diff(binEdges[key])/2 )

    binEdges = np.asarray( binEdgeList )
    print(energyCounts*binEdges)

def filterNan(realDose, energyCounts, energyList, energyListFile):
    realDoseFilt, energyCountsFilt, energyListFilt, energyListFileFilt, energyCountsFilt = [], [], [], [], []
    for i, doseItem in enumerate(realDose):
        if not np.isnan(doseItem):
            realDoseFilt.append( doseItem )
            energyCountsFilt.append( energyCounts[i] )
            energyListFilt.append( energyList[i] )
            energyListFileFilt.append( energyListFile[i] )
    return realDoseFilt, energyCountsFilt, energyListFilt, energyListFileFilt

def conversionFactorsToDf(conversionFactors):
    return pd.DataFrame(np.transpose(conversionFactors.reshape(3, 16)), columns = ['Slot%d' % i for i in range(1, 4)])

def evaluateData(inFolder, energyFile, header, conversionFactors, erlangen=False, angle=False, large=True):
    fittedConversionFactors = conversionFactors
    if angle:
        energyList, energyListFile, energyCounts, energyCountsErr, realDose, angleList = getCountsWrap(inFolder=inFolder, energyFile=energyFile, header=header, erlangen=erlangen, angle=angle, large=large)
    else:
        energyList, energyListFile, energyCounts, energyCountsErr, realDose = getCountsWrap(inFolder=inFolder, energyFile=energyFile, header=header, erlangen=erlangen, angle=angle, large=large)
    
    doseDfTemp = pd.read_table('%s/%s' % (inFolder, energyFile), comment='#')
    # energyMeanList = list( doseDfTemp['Energie'] )

    doseDict = getDose(energyCounts, conversionFactors)
    dose, doseErr, doseUnc = doseDict['dose'], doseDict['doseErr'], doseDict['doseUnc']
    # realDose = np.array( doseDfTemp[header[1]] )

    if angle:
        energyList = np.asarray(energyList)
        return pd.DataFrame(np.transpose([[round(item, 1) for item in energyList[:,0]], energyListFile, angleList, realDose, doseUnc]), columns=['Energy', 'MeanEnergy', 'Angle', 'RealDose', 'Dose'])
    else:
        return pd.DataFrame(np.transpose([[round(item, 1) for item in energyList], energyListFile, realDose, doseUnc]), columns=['Energy', 'MeanEnergy', 'RealDose', 'Dose'])
    
def plotDose(showDf, label, ylim=(0.6, 1.2), save=None):
    fig, ax = plt.subplots() # figsize=[10, 5])
    dose = np.asarray([item.n for item in showDf.Dose])
    doseErr = np.asarray([item.s for item in showDf.Dose])
    realDose = np.asarray(showDf.RealDose)

    energyMeanList = np.asarray( showDf['MeanEnergy'] )
    energyList = np.asarray( showDf['Energy'] )

    ax.errorbar(energyMeanList, dose/realDose, yerr=doseErr/realDose, ls='-', marker='x')
    ax.set_title(label)

    # Acceptance region
    plt.axhspan(0, 0.7, facecolor='red', alpha=0.1)
    plt.axhspan(0.7, 1.7, facecolor='green', alpha=0.1)
    plt.axhspan(1.7, 3, facecolor='red', alpha=0.1)
    ax.axhline(y=0.7, color='k', linewidth=.5)

    plt.ylim(ylim)

    # Perfect response
    ax.axhline(y=1, color='k', linestyle='--')

    ax.grid()
    ax.set_xlim(10, 2000)
    ax.set_xscale("log", nonposx='clip')

    ax.set_xlabel('Energy [kev]')
    ax.set_ylabel('Response')

    plt.grid(True,which="both",ls="-")
    # plt.legend(loc='lower right')

    if save:
        '''
        outName = save
        outName += '_resp_vs_energy'
        
        if HP0_07:
            plt.title(r'$H_\mathrm{p}(0.07)$')
            outName += '_0_07'
        else:
            plt.title(r'$H_\mathrm{p}(10)$')
            outName += '_10'
        '''
        # plt.savefig(outName + '.pdf')
        plt.savefig(save)
        